# Coinbase-recurring

## Principe
Une tâche (cron) se déclenche périodiquement, et place sur Coinbase des ordres d'achat de BTC et ETH avec les montants et la monnaie configurés.  
Il faut avoir un compte sur Coinbase, activer Coinbase Pro et créer une api key pour utiliser ce bot.

## Utilisation
- Créer le fichier src/main/resources/application.yml, selon le modèle suivant (exemple d'achat de 20 € de BTC et 10 € d'ETH le lundi à 12h)
```yaml
api:
  key: <key>
  secret: <secret>
  passphrase: <passphrase>

buy:
  cron: "0 0 12 * * MON"
  fiat: EUR
  btc: 20
  eth: 10
```
- Builder le projet
```bash
mvn clean install
```
- Lancer le jar généré
```bash
cd target
java -jar coinbase-recurring-0.0.0-SNAPSHOT-spring-boot.jar
```
