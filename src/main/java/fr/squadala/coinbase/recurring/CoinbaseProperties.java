package fr.squadala.coinbase.recurring;


import lombok.Getter;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.context.properties.ConstructorBinding;
import org.springframework.validation.annotation.Validated;

import javax.validation.constraints.NotNull;
import java.math.BigDecimal;


@ConstructorBinding
@ConfigurationProperties
@Validated
@Getter
public class CoinbaseProperties {


    @NotNull
    private final Api api;

    @NotNull
    private final Buy buy;

    public CoinbaseProperties(Api api, Buy buy) {
        this.api = api;
        this.buy = buy;
    }


    @Getter
    public static class Api {


        @NotNull
        private final String key;

        @NotNull
        private final String secret;

        @NotNull
        private final String passphrase;

        public Api(String key, String secret, String passphrase) {
            this.key = key;
            this.secret = secret;
            this.passphrase = passphrase;
        }
    }


    @Getter
    public static class Buy {

        @NotNull
        private final String cron;

        @NotNull
        private final String fiat;

        @NotNull
        private final BigDecimal btc;

        @NotNull
        private final BigDecimal eth;

        public Buy(String cron, String fiat, BigDecimal btc, BigDecimal eth) {
            this.cron = cron;
            this.fiat = fiat;
            this.btc = btc;
            this.eth = eth;
        }
    }

}
