package fr.squadala.coinbase.recurring.buy;

import fr.squadala.coinbase.recurring.CoinbaseProperties;
import fr.squadala.coinbase.recurring.client.AccountsApi;
import fr.squadala.coinbase.recurring.client.OrdersApi;
import fr.squadala.coinbase.recurring.client.ProductsApi;
import fr.squadala.coinbase.recurring.model.*;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.math.RoundingMode;

@Component
@Slf4j
@RequiredArgsConstructor
public class Buyer {

    private final CoinbaseProperties coinbaseProperties;
    private final AccountsApi accountsApi;
    private final ProductsApi productsApi;
    private final OrdersApi ordersApi;

    @Scheduled(cron = "${buy.cron}")
    public void run() {
        log.info("TIME TO BUY !");

        String fiat = coinbaseProperties.getBuy().getFiat();
        BigDecimal availableMoney = accountsApi.listAccounts().stream().filter(account -> fiat.equals(account.getCurrency()))
                .findAny()
                .map(Account::getBalance)
                .map(Double::parseDouble)
                .map(BigDecimal::valueOf)
                .orElse(BigDecimal.ZERO);
        log.info("Balance {} {}", availableMoney, fiat);

        BigDecimal btc = coinbaseProperties.getBuy().getBtc();
        BigDecimal eth = coinbaseProperties.getBuy().getEth();
        if (availableMoney.compareTo(btc.add(eth)) < 0) {
            log.error("NOT ENOUGH MONEY, YOU POOR !!!");
            return;
        }

        buy("BTC", btc, fiat);
        buy("ETH", eth, fiat);
    }

    private void buy(String crypto, BigDecimal amount, String fiat) {
        String productId = crypto + "-" + fiat;

        Ticker ticker = productsApi.productTicker(productId);
        log.debug(ticker.toString());

        BigDecimal price = BigDecimal.valueOf(Double.parseDouble(ticker.getPrice()));
        BigDecimal amountToBuy = amount.divide(price,8, RoundingMode.FLOOR);
        log.info("Buying {} {}", amountToBuy, crypto);

        OrderRequest buyRequest = new OrderRequest();
        buyRequest.setSide(OrderSide.BUY);
        buyRequest.setType(OrderType.MARKET);
        buyRequest.setProductId(productId);
        buyRequest.setSize(amountToBuy.toString());
        Order buy = ordersApi.placeOrder(buyRequest);
        log.debug(buy.toString());
    }

}

