package fr.squadala.coinbase.recurring.rest;


import fr.squadala.coinbase.recurring.CoinbaseProperties;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpRequest;
import org.springframework.http.client.ClientHttpRequestExecution;
import org.springframework.http.client.ClientHttpRequestInterceptor;
import org.springframework.http.client.ClientHttpResponse;

import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.time.Instant;
import java.util.Base64;


@RequiredArgsConstructor
public class AuthenticationInterceptor implements ClientHttpRequestInterceptor {


    private final CoinbaseProperties coinbaseProperties;


    @Override
    public ClientHttpResponse intercept(HttpRequest request, byte[] body, ClientHttpRequestExecution execution) throws IOException {
        String path = request.getURI().getPath();
        String query = request.getURI().getQuery();
        String requestPath = query != null ?
                path + "?" + query :
                path;
        String method = request.getMethodValue();
        String timestamp = String.valueOf(Instant.now().getEpochSecond());

        request.getHeaders().set("CB-ACCESS-KEY", coinbaseProperties.getApi().getKey());
        request.getHeaders().set("CB-ACCESS-SIGN", sign(timestamp, method, requestPath, body));
        request.getHeaders().set("CB-ACCESS-TIMESTAMP", timestamp);
        request.getHeaders().set("CB-ACCESS-PASSPHRASE", coinbaseProperties.getApi().getPassphrase());

        return execution.execute(request, body);
    }


    private String sign(String timestamp, String method, String requestPath, byte[] body) {

        byte[] secret = Base64.getDecoder().decode(coinbaseProperties.getApi().getSecret());

        String bodyString = new String(body, StandardCharsets.UTF_8);
        String prehash = timestamp + method + requestPath + bodyString;

        byte[] hmac = calcHmacSha256(secret, prehash.getBytes(StandardCharsets.UTF_8));

        return Base64.getEncoder().encodeToString(hmac);
    }


    private byte[] calcHmacSha256(byte[] secretKey, byte[] message) {
        byte[] hmacSha256;
        try {
            Mac mac = Mac.getInstance("HmacSHA256");
            SecretKeySpec secretKeySpec = new SecretKeySpec(secretKey, "HmacSHA256");
            mac.init(secretKeySpec);
            hmacSha256 = mac.doFinal(message);
        } catch (Exception e) {
            throw new RuntimeException("Failed to calculate hmac-sha256", e);
        }
        return hmacSha256;
    }
}
