package fr.squadala.coinbase.recurring.rest;


import fr.squadala.coinbase.recurring.CoinbaseProperties;
import lombok.RequiredArgsConstructor;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;


@Component
@RequiredArgsConstructor
public class RestTemplateProvider {

    private final CoinbaseProperties coinbaseProperties;


    @Bean
    public RestTemplate restTemplate() {
        RestTemplate restTemplate = new RestTemplate();
        restTemplate.getInterceptors().add(new AuthenticationInterceptor(coinbaseProperties));
        return restTemplate;
    }


}
