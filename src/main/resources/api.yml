openapi: 3.0.3

info:
  title: Coinbase API
  version: 0.0.1

servers:
  - url: https://api.pro.coinbase.com
  #- url: https://api-public.sandbox.pro.coinbase.com

tags:
  - name: Accounts
  - name: Products
  - name: Orders

paths:
  /time:
    get:
      summary: Get the API server time.
      operationId: getTime
      responses:
        200:
          description: Successful operation
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/time'

  /accounts:
    get:
      tags:
        - Accounts
      summary: Get a list of trading accounts from the profile of the API key.
      operationId: listAccounts
      responses:
        200:
          description: Successful operation
          content:
            application/json:
              schema:
                type: array
                items:
                  $ref: '#/components/schemas/account'
  /accounts/{account-id}:
    get:
      tags:
        - Accounts
      summary: Information for a single account. Use this endpoint when you know the account_id. API key must belong to the same profile as the account.
      operationId: getAccount
      parameters:
        - $ref: '#/components/parameters/path_parameter_account_id'
      responses:
        200:
          description: Successful operation
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/account'

  /products:
    get:
      tags:
        - Products
      summary: Get a list of available currency pairs for trading.
      operationId: listProducts
      responses:
        200:
          description: Successful operation
          content:
            application/json:
              schema:
                type: array
                items:
                  $ref: '#/components/schemas/product'
  /products/{product-id}:
    get:
      tags:
        - Products
      summary: Get market data for a specific currency pair.
      operationId: getProduct
      parameters:
        - $ref: '#/components/parameters/path_parameter_product_id'
      responses:
        200:
          description: Successful operation
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/product'
  /products/{product-id}/ticker:
    get:
      tags:
        - Products
      summary: Snapshot information about the last trade (tick), best bid/ask and 24h volume.
      operationId: productTicker
      parameters:
        - $ref: '#/components/parameters/path_parameter_product_id'
      responses:
        200:
          description: Successful operation
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/ticker'
  /products/{product-id}/stats:
    get:
      tags:
        - Products
      summary: Get 24 hr stats for the product. volume is in base currency units. open, high, low are in quote currency units.
      operationId: productStats
      parameters:
        - $ref: '#/components/parameters/path_parameter_product_id'
      responses:
        200:
          description: Successful operation
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/stats'
  /products/{product-id}/candles:
    get:
      tags:
        - Products
      summary: Historic rates for a product. Rates are returned in grouped buckets based on requested granularity.
      operationId: getHistoricRates
      parameters:
        - $ref: '#/components/parameters/path_parameter_product_id'
        - $ref: '#/components/parameters/query_parameter_start'
        - $ref: '#/components/parameters/query_parameter_end'
        - $ref: '#/components/parameters/query_parameter_granularity'
      responses:
        200:
          description: Successful operation
          content:
            application/json:
              schema:
                type: array
                items:
                  type: array
                  items:
                    type: string

  /orders:
    post:
      tags:
        - Orders
      summary: Place a New Order
      operationId: placeOrder
      requestBody:
        $ref: '#/components/requestBodies/order_body'
      responses:
        200:
          description: Successful operation
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/order'
    get:
      tags:
        - Orders
      summary: List Orders
      operationId: listOrders
      parameters:
        - $ref: '#/components/parameters/query_parameter_order_statut'
        - $ref: '#/components/parameters/query_parameter_product_id'
      responses:
        200:
          description: Successful operation
          content:
            application/json:
              schema:
                type: array
                items:
                  $ref: '#/components/schemas/order'


components:
  parameters:
    path_parameter_account_id:
      in: path
      name: account-id
      description: Account id
      required: true
      style: form
      explode: false
      schema:
        type: string
    path_parameter_product_id:
      in: path
      name: product-id
      description: Product id
      required: true
      style: form
      explode: false
      schema:
        type: string
    query_parameter_product_id:
      in: query
      name: product-id
      description: Product id
      required: false
      style: form
      explode: false
      schema:
        type: string
    query_parameter_order_statut:
      in: query
      name: status
      description: Order status
      required: false
      style: form
      explode: true
      schema:
        $ref: '#/components/schemas/order_status'
    query_parameter_start:
      in: query
      name: start
      description: Start
      required: false
      style: form
      explode: true
      schema:
        type: string
    query_parameter_end:
      in: query
      name: end
      description: End
      required: false
      style: form
      explode: true
      schema:
        type: string
    query_parameter_granularity:
      in: query
      name: granularity
      description: Granularity
      required: false
      style: form
      explode: true
      schema:
        $ref: '#/components/schemas/granularity'

  requestBodies:
    order_body:
      description: Order
      required: true
      content:
        application/json:
          schema:
            $ref: '#/components/schemas/order_request'

  schemas:
    time:
      type: object
      properties:
        iso:
          type: string
        epoch:
          type: string

    account:
      type: object
      required:
        - id
        - currency
        - balance
      properties:
        id:
          type: string
        currency:
          type: string
        balance:
          type: string
        available:
          type: string
        hold:
          type: string
        profile_id:
          type: string
        trading_enabled:
          type: boolean

    product:
      type: object
      required:
        - base_min_size
      properties:
        id:
          type: string
        display_name:
          type: string
        base_currency:
          type: string
        quote_currency:
          type: string
        base_increment:
          type: string
        quote_increment:
          type: string
        base_min_size:
          type: string
        base_max_size:
          type: string
        min_market_funds:
          type: string
        max_market_funds:
          type: string
        status:
          type: string
        status_message:
          type: string
        cancel_only:
          type: boolean
        limit_only:
          type: boolean
        post_only:
          type: boolean
        trading_disabled:
          type: boolean

    stats:
      type: object
      properties:
        open:
          type: string
        high:
          type: string
        low:
          type: string
        volume:
          type: string
        last:
          type: string
        volume_30day:
          type: string

    ticker:
      type: object
      required:
        - price
      properties:
        trade_id:
          type: integer
        price:
          type: string
        size:
          type: string
        bid:
          type: string
        ask:
          type: string
        volume:
          type: string
        time:
          type: string

    granularity:
      type: integer
      enum:
        - 60
        - 300
        - 900
        - 3600
        - 21600
        - 86400

    candle:
      type: object
      required:
        - time
        - low
        - high
        - open
        - close
        - volume
      properties:
        time:
          type: integer
        low:
          type: double
        high:
          type: double
        open:
          type: double
        close:
          type: double
        volume:
          type: double

    order_side:
      type: string
      enum:
        - buy
        - sell

    order_type:
      type: string
      enum:
        - limit
        - market

    order_status:
      type: string
      enum:
        - open
        - pending
        - active
        - all

    order_request:
      type: object
      properties:
        type:
          $ref: '#/components/schemas/order_type'
        side:
          $ref: '#/components/schemas/order_side'
        product_id:
          type: string
        price:
          type: string
        size:
          type: string


    order:
      type: object
      properties:
        id:
          type: string
        price:
          type: string
        size:
          type: string
        product_id:
          type: string
        side:
          $ref: '#/components/schemas/order_side'
        stp:
          type: string
        type:
          $ref: '#/components/schemas/order_type'
        time_in_force:
          type: string
        post_only:
          type: boolean
        created_at:
          type: string
        fill_fees:
          type: string
        filled_size:
          type: string
        executed_value:
          type: string
        status:
          $ref: '#/components/schemas/order_status'
        settled:
          type: boolean
